<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/prix_objets_periodes.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prix_objets_periodes_titre' => 'Périodes de prix',

	// T
	'titre_page_configurer_prix_objets_periodes' => 'Paramètres de périodes de prix'
);
