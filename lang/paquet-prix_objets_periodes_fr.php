<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/prix_objets_periodes.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prix_objets_periodes_description' => 'Adapter les prix selon une période de dates choisies',
	'prix_objets_periodes_nom' => 'Périodes de prix',
	'prix_objets_periodes_slogan' => 'Des prix selon la date'
);
