<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-prix_objets_periodes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prix_objets_periodes_description' => 'Adapt prices according to a chosen  period of dates', # MODIF
	'prix_objets_periodes_nom' => 'Price periods', # MODIF
	'prix_objets_periodes_slogan' => 'Prices according to the date'
);
