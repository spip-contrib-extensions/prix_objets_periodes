<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/prix_objets_periodes.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_po_periode' => 'Ajouter cette période de prix',

	// C
	'champ_criteres_label' => 'Critères de sélection : ',
	'champ_id_prix_extension_po_periode' => 'Choisissez une période pour ce prix :'
);
